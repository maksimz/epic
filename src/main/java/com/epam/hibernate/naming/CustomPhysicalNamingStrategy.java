package com.epam.hibernate.naming;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

/**
 * @author alex on 18/3/17.
 *         stackoverflow.com/documentation/hibernate/3051/custom-naming-strategy/23247/custom-physical-naming-strategy
 */
public class CustomPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl {

    public static final CustomPhysicalNamingStrategy INSTANCE = new CustomPhysicalNamingStrategy();

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return getIdentifier(name);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        return getIdentifier(name);
    }

    private Identifier getIdentifier(Identifier name) {
        String underscoredText = addUnderscores(name.getText());
        String upperCased = underscoredText.toUpperCase();
        return Identifier.toIdentifier(upperCased);
    }

    private static String addUnderscores(String name) {
        final StringBuilder buf = new StringBuilder(name);
        for (int i = 1; i < buf.length() - 1; i++) {
            if (Character.isLowerCase(buf.charAt(i - 1)) &&
                    Character.isUpperCase(buf.charAt(i)) &&
                    Character.isLowerCase(buf.charAt(i + 1))) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString();
    }
}
