package com.epam.hibernate.model.inher;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author alex on 4/3/17.
 */
@Entity
@DiscriminatorValue("S") // not needed if InheritanceType.TABLE_PER_CLASS
//@Table(name="SMALLPROJECT") // needed if InheritanceType.JOINED or InheritanceType.TABLE_PER_CLASS
public class SmallProject extends Project {
}
