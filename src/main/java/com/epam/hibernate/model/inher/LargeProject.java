package com.epam.hibernate.model.inher;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * @author alex on 4/3/17.
 */
@Entity
@DiscriminatorValue("L") // not needed if InheritanceType.TABLE_PER_CLASS
//@Table(name="LARGEPROJECT") // needed if InheritanceType.JOINED or InheritanceType.TABLE_PER_CLASS
public class LargeProject extends Project {

    private BigDecimal budget;
}
