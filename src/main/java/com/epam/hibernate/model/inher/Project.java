package com.epam.hibernate.model.inher;

import javax.persistence.*;

/**
 * Example from here: https://en.wikibooks.org/wiki/Java_Persistence/Inheritance
 *
 * @author alex on 4/3/17.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) //default, may be omitted
@DiscriminatorColumn(name = "PROJ_TYPE") // not needed if InheritanceType.TABLE_PER_CLASS
// @DiscriminatorOptions(force = true)
@Table(name = "PROJECT") // not needed if InheritanceType.TABLE_PER_CLASS
public abstract class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }
}
