package com.epam.hibernate.model.embed;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.sql.Date;

/**
 * @author alex on 4/3/17.
 */
@Embeddable
public class Period {

    @Column(name = "START_DATE")
    private java.sql.Date startDate;

    @Column(name = "END_DATE")
    private java.sql.Date endDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
