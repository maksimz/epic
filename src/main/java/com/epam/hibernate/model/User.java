package com.epam.hibernate.model;

import com.epam.hibernate.model.custom.AuditDate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * @author alex on 24/2/17.
 */
@Entity
// @Table(name = "USER") // it is best to use all uppercase names [if you wish your data model to be portable]
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //uses special IDENTITY column, increments on insertion [slow]
    // @Column(name = "ID") //no need due to CustomPhysicalNamingStrategy
    private int id;

    // Hibernate determines the datatype using reflection when the mapping files are processed.
    // This process adds overhead in terms of time and resources.
    // If startup performance is important, consider explicitly defining the type to use.
    @Column(/*name = "NAME",*/ unique = true, nullable = false, length = 254, columnDefinition = "varchar(254)")
    private String name;

    // @Column(name = "PHONE") // without specifying the column name it will be created the same as the field name
    private Integer phone;

    // Hibernate can't know if the java.util.Date property should map to a SQL DATE, TIME, or TIMESTAMP datatype.
    @Temporal(TemporalType.TIMESTAMP)
    private Date joinDate;

    // The default fetch type is LAZY for all relationships except for OneToOne and ManyToOne,
    // but in general it is a good idea to make every relationship LAZY
    // orphanRemoval = true: delete related objects when they are no longer referenced from the source
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CREDS_ID")
    private Credentials credentials;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID") //name of the artificial foreign key column
    private Role role;

    @Type(type = "com.epam.hibernate.model.custom.AuditDate")
    private AuditDate auditDate; //custom Hibernate type

    // @Embedded
    // private Period period;

    public User(String name, Integer phone, Credentials credentials, Role role) {
        this.name = name;
        this.phone = phone;
        setCredentials(credentials);
        setRole(role);
    }

    protected User() {
        // for Hibernate
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
        // in db there is bidirectional relationship, but java objects don't know it
        if (credentials.getUser() != this) credentials.setUser(this);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
        if (!role.getUsers().contains(this)) role.getUsers().add(this);
    }

    public AuditDate getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(AuditDate auditDate) {
        this.auditDate = auditDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone=" + phone +
                ", nick=" + credentials.getNickname() +
                ", role=" + role +
                '}';
    }
}
