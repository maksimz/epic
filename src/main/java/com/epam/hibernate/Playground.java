package com.epam.hibernate;

import com.epam.hibernate.model.Credentials;
import com.epam.hibernate.model.Role;
import com.epam.hibernate.model.User;
import com.epam.hibernate.model.custom.AuditDate;
import com.epam.hibernate.naming.CustomImplicitNamingStrategy;
import com.epam.hibernate.naming.CustomPhysicalNamingStrategy;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.EntityGraph;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author alex on 23/2/17.
 */
public class Playground {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        // JDBC

        //Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:epicdb", "SA", "");
        //Statement statement = connection.createStatement();
        //statement.executeUpdate("create table User (name varchar(255), phone int)");
        //statement.executeUpdate("insert into USER (id, name, phone) values (1, 'Alex', 1000000)");

        // ResultSet resultSet = statement.executeQuery("select * from USER");
        // while (resultSet.next()) {
        //     System.out.println(resultSet.getString(1)); //starts from 1
        //     System.out.println(resultSet.getString(2));
        // }


        // JPA

        // EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("epic-persistence-unit");
        // EntityManager em = emFactory.createEntityManager();
        // em.getTransaction().begin(); // only if the transaction-type in the persistence.xml is set to RESOURCE_LOCAL
        // em.persist(new User("Zulu", 9999999));
        // em.getTransaction().commit();
        // em.close();
        // emFactory.close();


        // Hibernate

        //JavaSE style, in JavaEE SessionFactory is injected (container-managed)
        Configuration cfg = new Configuration(); //create default config and service registry
        cfg.configure("META-INF/hibernate.cfg.xml"); //apply user config

        cfg.setPhysicalNamingStrategy(CustomPhysicalNamingStrategy.INSTANCE); //table/column name formatting
        cfg.setImplicitNamingStrategy(CustomImplicitNamingStrategy.INSTANCE); //foreign keys formatting

        SessionFactory sessionFactory = cfg.buildSessionFactory(); //create metadata with persistent classes
        Session session = sessionFactory.openSession(); //create persistent context to cache entities
        Transaction tx = session.beginTransaction();//open JDBC connection manually (resource local transaction, no need in JTA)

        //simple query breakdown
        User user = session.get(User.class, 1); //sql->RS->empty object(PojoInstantiator)->setID(SetterFieldImpl)->
        //->fetch relations(TwoPhaseLoad)->set each field(SetterFieldImpl)->repeat for all relations(AbstractRowReader)

        // get role USER from the db

        // JPA criteria style
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Role> q = cb.createQuery(Role.class);
        Root<Role> fromRole = q.from(Role.class);
        q.select(fromRole).where(cb.equal(fromRole.get("name"), Role.Title.USER));
        Role userRole = session.createQuery(q).getSingleResult();

        // HQL style
        Query query = session.createQuery("SELECT r FROM Role r WHERE r.name = :name");
        query.setParameter("name", Role.Title.USER);
        Role singleResult = (Role) query.getSingleResult();
        //

        Credentials deltaCreds = new Credentials("delta4", "Pass4");
        // no need to persist credentials separately, they'll be saved with the user due to 'cascade = CascadeType.ALL'
        User deltaUser = new User("Delta", 4000000, deltaCreds, userRole);
        session.save(deltaUser); //save cascade associations(CascadingActions)->iterate over getters & put values to array
        //(AbstractEntityTuplizer)->dehydrate(get FK id)(AbstractEntityPersister)->execute sql prepared statement->get id


        // session.getTransaction().commit(); // Hibernate flushes all changes inside transaction before any query


        // new typed JPA Criteria style
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> from = criteriaQuery.from(User.class);
        List<User> newCriteriaList = session.createQuery(criteriaQuery).getResultList();
        System.out.println("JPA Criteria style: " + newCriteriaList);

        // old Criteria style (deprecated since 5.2 Hibernate - use the new JPA Criteria)
        Criteria criteria = session.createCriteria(User.class);
        List oldCriteriaList = criteria.list();
        System.out.println("Old Hibernate Criteria style: " + oldCriteriaList);

        // HQL style
        List<User> hqlList = session.createQuery("from User", User.class).list();
        System.out.println("HQL style: " + hqlList);

        // Native SQL style
        List<User> sqlList = session.createNativeQuery("select * from USER", User.class).list();
        System.out.println("SQL style: " + sqlList);


        //check of the custom Hibernate type
        Credentials foxtrotCreds = new Credentials("foxtrot6", "Pass6");
        User foxtrot = new User("Foxtrot", 6000000, foxtrotCreds, userRole);
        foxtrot.setAuditDate(new AuditDate(new Date(), new Date()));
        session.save(foxtrot);
        System.out.println("Foxtrot created date: " + foxtrot.getAuditDate().getCreatedDate());


        session.clear(); //discard cached entities

        EntityGraph<?> entityGraph = session.getEntityGraph("role.users");
        Map<String, Object> hints = Collections.singletonMap("javax.persistence.fetchgraph", entityGraph);
        Role roleWithUsers = session.find(Role.class, 2, hints);
        // Hibernate uses the entity graph to create a load plan with all specified entities (Role, User and Credentials)
        // and load them with one query.

        session.close();
        // it is best to create a new Session/EntityManager for each transaction to avoid have stale objects remaining
        // in the persistence context, and to allow previously persistent/managed objects to garbage collect
        sessionFactory.close();
    }
}
