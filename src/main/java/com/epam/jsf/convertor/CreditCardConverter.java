package com.epam.jsf.convertor;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Created by Maksim on 2/17/2017.
 */
@FacesConverter("com.corejsf.Card")
public class CreditCardConverter implements Converter {

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s != null && s.contains("9")) {
            FacesMessage message = new FacesMessage(
                    "Could not convert data"
            );
            throw new ConverterException(message);
        }
        return "1233 1232 3333 3333 4444";
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return "1233 1232 3333 3333 4444";
    }
}
