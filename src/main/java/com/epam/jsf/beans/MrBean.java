package com.epam.jsf.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by Maksim on 1/30/2017.
 */
@ManagedBean(name = "mrBean")
@ViewScoped
public class MrBean implements Serializable {
    private String message;
    private String wifeMessage;

    // @ManagedProperty(value = "#{mrsBean}")
    private MrsBean mrsBean;

    public MrBean() {
        System.out.println("Mr Bean Constructor");
        this.message = " Hello! I'm Mr. Bean";
    }

    @PostConstruct
    public void init() {
        System.out.println("Post construct");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Destroy");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MrsBean getMrsBean() {
        return mrsBean;
    }

    public void setMrsBean(MrsBean mrsBean) {
        this.mrsBean = mrsBean;
    }

    public String getWifeMessage() {
        return wifeMessage;
    }

    public void setWifeMessage(String wifeMessage) {
        this.wifeMessage = wifeMessage;
    }
}
