package com.epam.jsf.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Maksim on 2/4/2017.
 */
@ManagedBean(name = "payment")
@RequestScoped
public class PaymentBean implements Serializable {
    private Double amount;
    //  @Size(min=13,  message = "Card number is incorrect!")
    private String card;
    private Date date = new Date();

    public PaymentBean() {
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
