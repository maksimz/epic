package com.epam.jsf.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import java.io.Serializable;

/**
 * Created by Maksim on 2/4/2017.
 */
@ManagedBean(name = "user")
@ViewScoped
public class User implements Serializable {
    private String name;
    private String password;
    private String welcomeMessage;

    public User() {
        welcomeMessage = "have visited a server by AJAX";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String checkCredentials() {
        if ("cheater".equals(name)) {
            return "forbidden";
        } else {
            return "success";
        }
    }

    public String getWelcomeMessage() {

        return name + " " + welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public void dataListener(AjaxBehaviorEvent event) {
        System.out.println("Hello moto");
    }
}
