package com.epam.jsf.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Created by Maksim on 1/30/2017.
 */
@ManagedBean(name = "mrsBean")
@SessionScoped
public class MrsBean implements Serializable {
    private String message;

    public MrsBean() {
        System.out.println("Mrs. Bean Constructor");
        this.message = "Hello! I'm Mrs. Bean!";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
